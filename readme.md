[freeCodeCamp's personal portfolio page project](https://beta.freecodecamp.org/en/challenges/applied-responsive-web-design-projects/build-a-personal-portfolio-webpage)

This was completed for the [personal portfolio page deadline](https://forum.freecodecamp.org/t/build-a-personal-portfolio-webpage-project-questions-discussions-and-resources-january-2018-cohort/180424?u=camper) of the [January 2018 fCC Cohort](https://forum.freecodecamp.org/t/january-2018-fcc-cohort-starting-soon/165977?u=camper).

Link to the [Live project](https://camper-fcc.gitlab.io/beta-portfolio/)

[fCC Forum Post](https://www.freecodecamp.org/forum/t/beta-curriculum-personal-portfolio-page-can-you-spare-a-review/182085)

Here are some details of my project:

- CSS Grid and media queries for fully responsive, mobile-first design
- Uses CSS Custom Properties (Variables)
- [No inline styles](https://www.thoughtco.com/avoid-inline-styles-for-css-3466846)
- [No CSS ID selectors for styling](http://oli.jp/2011/ids/)
- Uses [semantic HTML](https://internetingishard.com/html-and-css/semantic-html/) - [fCC Guide on Semantic HTML](https://guide.freecodecamp.org/html/html5-semantic-elements)
- Custom GPLv3 JavaScript
- Passes [fCC Beta tests](https://beta.freecodecamp.org/en/challenges/applied-responsive-web-design-projects/build-a-personal-portfolio-webpage)
- Hosted on [GitLab Pages](https://about.gitlab.com/features/pages/)
- Includes Favicon
- Uses [Modern-Normalize.css](https://github.com/sindresorhus/modern-normalize)
- Color scheme based on [Botticelli/White](https://pigment.shapefactory.co/?a=C7E0EC&b=FFFEFF)
- Fonts based on [Ovo, Muli](https://femmebot.github.io/google-type/)
- Valid HTML5 according to the [HTML5 Validator](https://validator.w3.org/nu/?showsource=yes&doc=https%3A%2F%2Fcamper-fcc.gitlab.io%2Fbeta-portfolio%2F)
- Valid CSS3 according to the [CSS3 Validator](https://jigsaw.w3.org/css-validator/validator?uri=https%3A%2F%2Fcamper-fcc.gitlab.io%2Fbeta-portfolio%2F&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=en) (_Doesn't recognize CSS Custom Properties_)
- A11y ready based on the [WAVE Accessibility report](http://wave.webaim.org/report#/https://camper-fcc.gitlab.io/beta-portfolio/)

Thank you for your time and feedback! :sunny: